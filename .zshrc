# #######################################
# .zshrc
# #######################################

# Path to your oh-my-zsh configuration.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
#export ZSH_THEME="robbyrussell"
export ZSH_THEME="jim"

source $ZSH/oh-my-zsh.sh

# Source local config
if [ -f $HOME/.zshrc.local ]; then
    source $HOME/.zshrc.local
fi

# Source go completions
if [ -f $GOROOT/misc/zsh/go ]; then
    source $GOROOT/misc/zsh/go
fi

# gc
prefixes=(5 6 8)
for p in $prefixes; do
        compctl -g "*.${p}" ${p}l
        compctl -g "*.go" ${p}g
done

export PATH=$PATH:$HOME/bin

# standard go tools
compctl -g "*.go" gofmt

# gccgo
compctl -g "*.go" gccgo

alias get=git
alias la='ls -alh'

### Options
setopt extendedglob

setopt no_beep

setopt autocd # Evaluating a directory name cds to said directory
setopt auto_pushd
setopt pushd_ignore_dups
setopt pushd_silent

setopt hist_ignore_dups
setopt extended_history
setopt inc_append_history
setopt hist_fcntl_lock
setopt HIST_IGNORE_SPACE # Can't see me!

setopt c_bases # Outputs hex and octal prettily

# Use vi keybindings
set -o vi


PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
