set nocompatible               " be iMproved
set mouse=a
set shell=/bin/sh 

" Vundle stuff
filetype off                   " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required! 
Bundle 'gmarik/vundle'

" My Bundles here:
" original repos on github 
Bundle 'bling/vim-airline'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'tpope/vim-fugitive'
Bundle 'scrooloose/syntastic'
Bundle 'nsf/gocode'
Bundle 'kevinw/pyflakes-vim'
Bundle 'scrooloose/nerdtree'
" Bundle 'vim-scripts/taglist.vim'
Bundle 'fs111/pydoc.vim'
Bundle 'vim-scripts/TaskList.vim'
Bundle 'vim-scripts/AutoComplPop'
Bundle 'ervandew/supertab'
Bundle 'Rip-Rip/clang_complete'
Bundle 'vim-scripts/pep8'
Bundle 'yodiaditya/vim-pydjango'
" Required for snipmate
Bundle 'garbas/vim-snipmate'
Bundle "tomtom/tlib_vim"
Bundle "snipmate-snippets"
Bundle 'MarcWeber/vim-addon-mw-utils'
" End snipmate

" vim-scripts repos
" Bundle 'name_of_plugin'

" non github repos
Bundle 'git://git.wincent.com/command-t.git'

" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install(update) bundles
" :BundleSearch(!) foo - search(or refresh cache first) for foo
" :BundleClean(!)      - confirm(or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle command are not allowed..
filetype on
filetype plugin on
filetype plugin indent on     " required for Vundle

set t_Co=256
syntax on
set autoindent
set smarttab
set laststatus=2 " Show status line (for Powerline)

" Code folding
" 'za' to open and close folds
set foldmethod=indent
set foldlevel=99

" Use control to jump around panes
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h
let g:pyflakes_use_quickfix = 0

" Color schemes installed:
" http://www.vi-improved.org/color_sampler_pack/
colorscheme wombat256

set number
set numberwidth=4
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE

" Python Settings
au BufRead,BufNewFile *.py,*pyw set shiftwidth=4
au BufRead,BufNewFile *.py,*pyw set tabstop=4

" C/C++/Go/Java
au BufRead,BufNewFile *.go set filetype=go
au BufRead,BufNewFile *.cpp set filetype=cpp
au BufRead,BufNewFile *.c set filetype=c

au BufRead,BufNewFile *.c,*.cpp,*.go set shiftwidth=4
au BufRead,BufNewFile *.c,*.cpp,*.go set tabstop=4
au BufNewFile,BufRead,BufEnter *.cpp,*.hpp set omnifunc=omni#cpp#complete#Main
au BufNewFile,BufRead,BufEnter *.cpp,*.hpp set omnifunc=omni#c#complete#Main

augroup filetype
  au! BufRead,BufNewFile *.proto setfiletype proto
augroup end

set backspace=indent,eol,start
set expandtab
au FileType make set noexpandtab
au FileType go set noexpandtab

" Include local vim config
if filereadable(expand("~/.vimrc.local"))
  source ~/.vimrc.local
endif

" This beauty remembers where you were the last time you edited the file,
" and returns to the same position.
au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif""'")'")

" NERDTree Settings
" Open vim without specifying a file, display NERDTree
autocmd vimenter * if !argc() | NERDTree | endif
" Close vim automatically if NERDTree is the last window open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
" ctrl+n open NERDTree using path of currently active buffer
map <silent> <c-n> :NERDTreeToggle %:p:h<CR>

" EasyMotion
let g:EasyMotion_leader_key = '<Leader><Leader>' 

" Taglist
" Exit if Tlist is the only window open
let Tlist_Exit_OnlyWindow = 1 
" Generate tags even with taglist window closed
let Tlist_Process_File_Always = 1

" Function key remaps
" F8 toggle taglist window
nnoremap <silent> <F8> :TlistToggle<CR>

" Super Tab
let g:SuperTabDefaultCompletionType = "context"

" OmniComplete
set completeopt=menuone,longest,preview

" Disable auto popup, use <Tab> to autocomplete
let g:clang_complete_auto = 1
" Show clang errors in the quickfix window
let g:clang_complete_copn = 1

" Pep8
let g:pep8_map='<leader>8'

" Golang stuff
function GoFormat()
        let rel=line(".")
        %!/usr/bin/env gofmt
        call cursor(rel, 1)
endfunction

autocmd Filetype go command! Fmt call GoFormat() 

" Vimbits additions
" =================
map Y y$ 

" Improve up/down movement on wrapped lines
nnoremap j gj
nnoremap k gk

" Auto reload .vimrc when it is saved
au BufWritePost .vimrc so ~/.vimrc 

" django stuff
let $DJANGO_SETTINGS_MODULE='dms.settings'
nnoremap <leader>ya ggvG$"zy
