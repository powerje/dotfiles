#!/bin/sh

# Setup symbolic links based on OS

if [ $# -ne 1 ]
then
  echo "Usage: setup_links.sh [osx | linux]"
  exit
fi

ln -s $(pwd -P)/.zshrc ~/.zshrc
ln -s $(pwd -P)/.vimrc ~/.vimrc
ln -s $(pwd -P)/.vim ~/.vim
ln -s $(pwd -P)/.oh-my-zsh ~/.oh-my-zsh
ln -s $(pwd -P)/.zshrc.local.$1 ~/.zshrc.local
ln -s $(pwd -P)/.bashrc ~/.bashrc
