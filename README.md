Jim's dot files
===============

Use the 'setup_links.sh' script to setup symbolc links.

Vim management is handled by vundle, open vim and do :BundleInstall to update plugins.

Setup to use zsh/oh-my-zsh. Platform specific stuff is in:
'.dotfile.local.platform'
where platform is either linux or osx.

